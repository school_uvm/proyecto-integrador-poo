package school;

import school.menu.Menu;
import school.menu.MenuItem;
import school.menu.RunnableSafe;

import java.util.ArrayList;
import java.util.List;

public final class MenuPrincipal extends Menu {

	/* ---------------------------------------------------------------
	 * Propiedades
	 * ------------------------------------------------------------ */

	/**
	 * Menu para los profesores
	 */
	private final Menu profesorMenu = new Menu("Profesores");

	/**
	 * Menu para las materias
	 */
	private final Menu materiaMenu = new Menu("Materias");

	/**
	 * Menu para los alumnos
	 */
	private final Menu alumnosMenu = new Menu("Alumnos");

	/**
	 * Menu para modificar la materia de los alumnos
	 */
	private final Menu alumnoMateriasMenu = new Menu("Alumno Materias");

	/* ---------------------------------------------------------------
	 * Constructores
	 * ------------------------------------------------------------ */

	/**
	 * Constructor por defecto
	 */
	public MenuPrincipal() {
		super("Menú Principal");
		// Insertamos los menus internos
		agregarElemento(profesorMenu);
		agregarElemento(materiaMenu);
		agregarElemento(alumnosMenu);
	}

	/* ---------------------------------------------------------------
	 * Profesores
	 * ------------------------------------------------------------ */

	/**
	 * Todos los elementos del menu profesor
	 */
	private final List<MenuItem> elementosProfesores = new ArrayList<>();

	/**
	 * Inicializamos las acciones del profesor
	 *
	 * @param agregar  la acción para agregar profesores
	 * @param eliminar la acción para eliminar profesores
	 * @param mostrar  la acción para mostrar los profesores
	 */
	public void inicializarProfesores(RunnableSafe agregar, RunnableSafe eliminar, RunnableSafe mostrar) {
		// Verificar si los elementos son válidos
		if (agregar != null)
			elementosProfesores.add(new MenuItem("Agregar Profesor", agregar));
		if (eliminar != null)
			elementosProfesores.add(new MenuItem("Eliminar Profesor", eliminar));
		if (mostrar != null)
			elementosProfesores.add(new MenuItem("Mostrar Profesores", mostrar));

		// Agregamos los elementos al menu.
		// Este tipo de operación se llama paso de métodos por referencia
		// lo que hace más fácil el llamado de operaciones y evita el uso de expresiones lambda como la siguiente:
		// elementosProfesores.forEach(it -> profesorMenu.agregarElemento(it));
		// como se puede ver esto simplifica bastante el código.
		elementosProfesores.forEach(profesorMenu::agregarElemento);
	}

	/**
	 * Todos los elementos del menu materias
	 */
	private final List<MenuItem> elementosMaterias = new ArrayList<>();

	/**
	 * Inicializamos las acciones de las materias
	 *
	 * @param agregar  la acción para agregar materias
	 * @param eliminar la acción para eliminar materias
	 * @param mostrar  la acción para mostrar las materias
	 */
	public void inicializarMaterias(RunnableSafe agregar, RunnableSafe eliminar, RunnableSafe mostrar) {
		// Verificar si los elementos son válidos
		if (agregar != null)
			elementosMaterias.add(new MenuItem("Agregar Materia", agregar));
		if (eliminar != null)
			elementosMaterias.add(new MenuItem("Eliminar Materia", eliminar));
		if (mostrar != null)
			elementosMaterias.add(new MenuItem("Mostrar Materias", mostrar));

		// Agregamos los elementos al menu.
		// Este tipo de operación se llama paso de métodos por referencia
		// lo que hace más fácil el llamado de operaciones y evita el uso de expresiones lambda como la siguiente:
		// elementosMaterias.forEach(it -> materiaMenu.agregarElemento(it));
		// como se puede ver esto simplifica bastante el código.
		elementosMaterias.forEach(materiaMenu::agregarElemento);
	}

	/**
	 * Todos los elementos del menu alumnos
	 */
	private final List<MenuItem> elementosAlumnos = new ArrayList<>();

	/**
	 * Inicializamos las acciones de los alumnos
	 *
	 * @param agregar  la acción para agregar alumnos
	 * @param eliminar la acción para eliminar alumnos
	 * @param mostrar  la acción para mostrar los alumnos
	 */
	public void inicializarAlumnos(
		RunnableSafe agregar,
		RunnableSafe eliminar,
		RunnableSafe mostrar,
		RunnableSafe agregarMateria,
		RunnableSafe quitarMateria) {
		// Verificar si los elementos son válidos
		if (agregar != null)
			elementosAlumnos.add(new MenuItem("Agregar Alumno", agregar));
		if (eliminar != null)
			elementosAlumnos.add(new MenuItem("Eliminar Alumno", eliminar));
		if (mostrar != null)
			elementosAlumnos.add(new MenuItem("Mostrar Alumnos", mostrar));

		// Las acciones de las materias del alumno
		alumnoMateriasMenu.agregarElemento(new MenuItem("Agregar materia", agregarMateria));
		alumnoMateriasMenu.agregarElemento(new MenuItem("Quitar materia", quitarMateria));

		// Agregamos el menu anidado dentro del menu alumnos
		elementosAlumnos.add(alumnoMateriasMenu);

		// Agregamos los elementos al menu.
		// Este tipo de operación se llama paso de métodos por referencia
		// lo que hace más fácil el llamado de operaciones y evita el uso de expresiones lambda como la siguiente:
		// elementosAlumno.forEach(it -> alumnosMenu.agregarElemento(it));
		// como se puede ver esto simplifica bastante el código.
		elementosAlumnos.forEach(alumnosMenu::agregarElemento);
	}

}
