package school;

public final class Mensajes {

	/**
	 * This class cannot be instantiated
	 */
	private Mensajes() {
	}

	/* ---------------------------------------------------------------
	 * Propiedades
	 * ------------------------------------------------------------ */

	/**
	 * Mensaje de error vació
	 */
	public static String MSG_ERR_VACIO = "El contenido no debe estar vació";

	/**
	 * Mensaje de error numérico
	 */
	public static String MSG_ERR_NUMERO = "El contenido no es un numero valido";

	/**
	 * Mensaje de error cuando una acción es cancelada
	 */
	public static String MSG_ERR_CANCEL = "Acción cancelada";

	/**
	 * Mensaje de error lista inválido
	 */
	public static String MSG_ERR_LISTA = "La opción no es valida";

	/**
	 * Mensaje de verificación
	 */
	public static String MSG_VERIFCACION = "Esta realmente seguro de realizar esta acción (La información se perderá)";

	/* ---------------------------------------------------------------
	 * Métodos
	 * ------------------------------------------------------------ */

	/**
	 * Mensaje de error para los elementos enumerados
	 *
	 * @param articulo   el artículo del sustantivo
	 * @param sustantivo el sustantivo a evaluar
	 * @return el mensaje de error
	 */
	public static String msgEnumErr(String articulo, String sustantivo) {
		return String.format("%s %s seleccionado no es valido", articulo, sustantivo);
	}

	public static String msgIn(String articulo, String sustantivo, String propiedad, String articuloProp) {
		return String.format("Ingresa %s %s %s %s", articulo, propiedad, articuloProp, sustantivo);
	}

	public static String msgIn(String articulo, String sustantivo, String propiedad) {
		return msgIn(articulo, sustantivo, propiedad, "del");
	}

}
