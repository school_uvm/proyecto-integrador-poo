package school.elementos;

import school.elementos.base.Identificable;
import school.elementos.base.Info;

public final class Materia extends Identificable implements Info {

	/**
	 * Nombre de la materia
	 */
	private final String nombre;

	/**
	 * Total de créditos para aprobar la materia
	 */
	private final int creditos;

	/**
	 * El profesor que imparte la materia
	 */
	private Profesor profesor;

	/* -----------------------------------------------------
	 * Constructores
	 * ----------------------------------------------------- */

	/**
	 * Constructor vació
	 */
	public Materia() {
		this(-1L, "", -1, null);
	}

	/**
	 * Constructor por defecto
	 *
	 * @param nombre   nombre de la materia
	 * @param creditos puntos para acreditar la materia
	 * @param profesor el profesor que imparte la materia
	 */
	public Materia(long id, String nombre, int creditos, Profesor profesor) {
		// Llamamos el constructor padre
		super(id);
		// Inicializamos las propiedades
		this.nombre = nombre;
		this.creditos = creditos;
		this.profesor = profesor;
	}

	/* -----------------------------------------------------
	 * Methods
	 * ----------------------------------------------------- */

	/**
	 * Devuelve el nombre de la materia
	 *
	 * @return el nombre de la materia
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Devuelve los créditos de la materia
	 *
	 * @return los créditos de la materia
	 */
	public int getCreditos() {
		return creditos;
	}

	/**
	 * Devuelve el profesor de la materia
	 *
	 * @return el profesor que imparte la materia
	 */
	public Profesor getProfesor() {
		return profesor;
	}

	/**
	 * Cambiamos el profesor
	 *
	 * @param profesor el nuevo profesor
	 */
	public void setProfesor(Profesor profesor) {
		this.profesor = profesor;
	}

	/**
	 * Devuelve la información del objeto actual
	 *
	 * @return la información del objeto
	 */
	@SuppressWarnings("StringBufferReplaceableByString")
	@Override
	public String getInformacion() {
		StringBuilder builder = new StringBuilder();

		// Insertamos la información más relevante
		builder.append(getNombre()).append(": ").append('\n')
			.append('\t').append("créditos: ").append(getCreditos()).append('\n')
			.append('\t').append("profesor: ").append(getProfesor() == null ? null : getProfesor().getInformacion())
			.append('\n');

		return builder.toString();
	}

}
