package school.elementos;


import school.elementos.base.Persona;

import java.util.Locale;

public final class Profesor extends Persona {

	/**
	 * Título o profesión del profesor
	 */
	private final String profesion;

	/**
	 * Cédula profesional
	 */
	private final String cedula;

	/* -----------------------------------------------------
	 * Constructores
	 * ----------------------------------------------------- */

	/**
	 * Constructor vacío
	 */
	public Profesor() {
		this(-1L, "", "", -1, Sexo.MASCULINO, "", "");
	}

	/**
	 * Constructor por defecto para una persona
	 *
	 * @param nombre    el nombre de la persona
	 * @param apellido  el apellido de la persona
	 * @param edad      la edad de la persona
	 * @param sexo      el sexo de la persona
	 * @param profesion titulo o profesión
	 * @param cedula    cédula profesional
	 * @see Sexo
	 */
	public Profesor(long id, String nombre, String apellido, Number edad, Sexo sexo, String profesion, String cedula) {
		// Llamamos el constructor de la clase padre
		super(id, nombre, apellido, edad, sexo);
		// Iniciamos las propiedades
		this.profesion = profesion;
		this.cedula = cedula.toUpperCase(Locale.ROOT);
	}

	/* -----------------------------------------------------
	 * Métodos
	 * ----------------------------------------------------- */

	/**
	 * Devuelve el título o profesión del profesor
	 *
	 * @return el título o profesión del profesor
	 */
	public String getProfesion() {
		return profesion;
	}

	/**
	 * Devuelve la cédula profesional del profesor
	 *
	 * @return la cédula profesional del profesor
	 */
	public String getCedula() {
		return cedula;
	}

	/**
	 * Información extra para mostrar
	 *
	 * @param builder objeto que se debe manipular
	 */
	@Override
	protected void putExtraInformacion(StringBuilder builder) {
		builder.append('\t').append("id: ").append(getId()).append('\n')
			.append('\t').append("profesión: ").append(getProfesion()).append('\n')
			.append('\t').append("cédula: ").append(getCedula());
	}

}
