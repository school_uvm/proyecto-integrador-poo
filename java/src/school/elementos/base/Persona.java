package school.elementos.base;

public abstract class Persona extends Identificable implements Info {

	/**
	 * Nombre de la persona
	 */
	private String nombre;

	/**
	 * Apellido de la persona
	 */
	private String apellido;

	/**
	 * Edad de la persona
	 */
	private short edad;

	/**
	 * Sexo de la persona
	 */
	private Sexo sexo;


	/* -----------------------------------------------------
	 * Constructores
	 * ----------------------------------------------------- */

	/**
	 * Constructor vació y únicamente usado para llenarlo por importación de datos
	 */
	public Persona() {
		this(-1L, "", "", 0, Sexo.MASCULINO);
	}

	/**
	 * Constructor por defecto para una persona
	 *
	 * @param id       el id de la persona
	 * @param nombre   el nombre de la persona
	 * @param apellido el apellido de la persona
	 * @param edad     la edad de la persona
	 * @param sexo     el sexo de la persona
	 * @see Sexo
	 */
	public Persona(long id, String nombre, String apellido, Number edad, Sexo sexo) {
		// Llamamos el constructor padre
		super(id);
		// Iniciamos las propiedades
		this.nombre = nombre;
		this.apellido = apellido;
		this.edad = edad.shortValue();
		this.sexo = sexo;
	}

	/* -----------------------------------------------------
	 * Métodos
	 * ----------------------------------------------------- */

	/**
	 * Devuelve el nombre de la persona
	 *
	 * @return el nombre de la persona
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Devuelve el apellido de la persona
	 *
	 * @return el apellido de la persona
	 */
	public String getApellido() {
		return apellido;
	}

	/**
	 * Devuelve la edad de la persona
	 *
	 * @return la edad de la persona
	 */
	public short getEdad() {
		return edad;
	}

	/**
	 * Devuelve el sexo de la persona
	 *
	 * @return el sexo de la persona
	 */
	public Sexo getSexo() {
		return sexo;
	}

	/**
	 * Cambia el nombre del objeto actual.
	 * Este método es protegido debido a que la interfaz importar debe poder modificar el contenido,
	 * pero no puede ser modificado por algún agente público
	 *
	 * @param nombre el nombre destino
	 */
	protected void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * Cambia el apellido del objeto actual.
	 * Este método es protegido debido a que la interfaz importar debe poder modificar el contenido,
	 * pero no puede ser modificado por algún agente público
	 *
	 * @param apellido el apellido destino
	 */
	protected void setApellido(String apellido) {
		this.apellido = apellido;
	}

	/**
	 * Cambia la edad del objeto actual.
	 * Este método es protegido debido a que la interfaz importar debe poder modificar el contenido,
	 * pero no puede ser modificado por algún agente público
	 *
	 * @param edad la edad destino
	 */
	protected void setEdad(Number edad) {
		this.edad = edad.shortValue();
	}

	/**
	 * Cambia el sexo del objeto actual.
	 * Este método es protegido debido a que la interfaz importar debe poder modificar el contenido,
	 * pero no puede ser modificado por algún agente público
	 *
	 * @param sexo el sexo destino
	 */
	protected void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}

	/**
	 * Obtenemos la información del objeto
	 *
	 * @return la información del objeto
	 */
	@Override
	public String getInformacion() {
		// Generamos un creador de contenido
		StringBuilder builder = new StringBuilder();

		// Información importante
		builder.append(getNombre()).append(" ").append(getApellido()).append(": ").append('\n')
			.append('\t').append("edad: ").append(getEdad()).append('\n')
			.append('\t').append("sexo: ").append(getSexo().identificador);

		// Insertamos la información extra
		putExtraInformacion(builder);
		return builder.toString();
	}

	/**
	 * Información extra para mostrar
	 *
	 * @param builder objeto que se debe manipular
	 */
	protected abstract void putExtraInformacion(StringBuilder builder);

	/* -----------------------------------------------------
	 * Estructuras internas
	 * ----------------------------------------------------- */

	public enum Sexo {

		/**
		 * Sexo masculino
		 */
		MASCULINO('M'),

		/**
		 * Sexo femenino
		 */
		FEMENINO('F');

		/**
		 * Identificador
		 */
		public final char identificador;

		/**
		 * Constructor del tipo enumerado
		 *
		 * @param id identificador para el tipo enumerado
		 */
		Sexo(char id) {
			identificador = id;
		}

	}

}
