package school.elementos.base;

public abstract class Identificable {

	/* ---------------------------------------------------------------
	 * Propiedades
	 * ------------------------------------------------------------ */

	/**
	 * El identificador del objeto
	 */
	private long id;

	/* ---------------------------------------------------------------
	 * Constructores
	 * ------------------------------------------------------------ */

	/**
	 * Constructor por defecto
	 *
	 * @param id el identificador del objeto
	 */
	public Identificable(long id) {
		this.id = id;
	}

	/* ---------------------------------------------------------------
	 * Métodos
	 * ------------------------------------------------------------ */

	/**
	 * Devuelve el id del objeto
	 *
	 * @return el identificador del objeto
	 */
	public long getId() {
		return id;
	}

	/**
	 * Cambia el identificador del objeto
	 *
	 * @param nuevoId el nuevo identificador del objeto
	 */
	protected void setId(long nuevoId) {
		id = nuevoId;
	}

}
