package school.elementos.base;

public interface Info {

	/**
	 * Devuelve la información del objeto actual
	 *
	 * @return la información del objeto
	 */
	String getInformacion();

}
