package school.elementos;

import school.elementos.base.Persona;

import java.util.LinkedList;
import java.util.List;

public final class Alumno extends Persona {

	/**
	 * Número de control escolar
	 */
	private final long noControl;

	/**
	 * Semestre del alumno
	 */
	private final short semestre;

	/**
	 * Lista de materias del alumno
	 */
	private final List<Materia> materias;

	/* -----------------------------------------------------
	 * Constructores
	 * ----------------------------------------------------- */

	/**
	 * Constructor vació y únicamente usado para llenarlo por importación de datos
	 */
	public Alumno() {
		this(-1L, "", "", -1, Sexo.MASCULINO, -1, -1, null);
	}

	/**
	 * Constructor por defecto para un alumno
	 *
	 * @param id        el id del alumno
	 * @param nombre    el nombre de la persona
	 * @param apellido  el apellido de la persona
	 * @param edad      la edad de la persona
	 * @param sexo      el sexo de la persona
	 * @param noControl el número de control o id
	 * @param semestre  el semestre del alumno
	 * @param materias  lista de materias del alumno
	 * @see Sexo
	 */
	public Alumno(long id, String nombre, String apellido, Number edad, Sexo sexo, Number noControl, Number semestre,
				  List<Materia> materias) {
		// Llamamos el constructor de la clase padre
		super(id, nombre, apellido, edad, sexo);
		// Iniciamos las propiedades
		this.noControl = noControl.longValue();
		this.semestre = semestre.shortValue();
		this.materias = new LinkedList<>();

		// Verificamos que las materias no sean nulas
		if (materias != null) this.materias.addAll(materias);
	}

	/* -----------------------------------------------------
	 * Métodos
	 * ----------------------------------------------------- */

	public long getNoControl() {
		return noControl;
	}

	public short getSemestre() {
		return semestre;
	}

	public List<Materia> getMaterias() {
		return materias;
	}

	/**
	 * Información extra para mostrar
	 *
	 * @param builder objeto que se debe manipular
	 */
	@Override
	protected void putExtraInformacion(StringBuilder builder) {
		builder.append('\t').append("id: ").append(getId()).append('\n')
			.append('\t').append("noControl: ").append(getNoControl()).append('\n')
			.append('\t').append("semestre: ").append(getSemestre()).append('\n');

		if (materias == null) return;

		builder.append("materias: ");
		for (Materia materia : materias) {
			builder.append(materia.getInformacion())
				.append('\n');
		}
	}

}
