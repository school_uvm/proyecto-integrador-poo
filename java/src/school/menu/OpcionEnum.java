package school.menu;

/**
 * Tipo enumerado que guarda dos valores.
 * Verdadero o falso
 */
public enum OpcionEnum {
	SI,
	NO
}
