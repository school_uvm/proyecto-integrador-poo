package school.menu;

import school.error.InfoException;

import java.util.logging.Level;
import java.util.logging.Logger;

public interface RunnableSafe extends Runnable {

	/**
	 * Current object logging system
	 */
	Logger LOG = Logger.getLogger(RunnableSafe.class.getName());

	/**
	 * Ejecución de la interfaz pero con posibilidad de errores
	 *
	 * @throws Exception Error al ejecutar la acción
	 */
	void runErr() throws Exception;

	/**
	 * La ejecución de la interfaz sin errores
	 */
	@Override
	default void run() {
		try {
			runErr();
		} catch (InfoException e) {
			LOG.log(Level.SEVERE, e.getMessage());
		} catch (Exception e) {
			LOG.log(Level.SEVERE, "Error al ejecutar la acción", e);
		}
	}

}
