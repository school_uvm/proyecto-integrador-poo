package school.menu;

import school.utilidades.EntradaUtils;

import java.io.BufferedReader;
import java.io.IOException;

public class Menu extends MenuItem {

	/* ---------------------------------------------------------------
	 * Propiedades
	 * ------------------------------------------------------------ */

	/**
	 * Cantidad de elementos que tiene el menu
	 */
	private int cantidad = 1;

	/**
	 * Determina si el menú está activo
	 */
	private boolean activo;

	/**
	 * Guardamos todos los elementos del menú
	 */
	private MenuItem[] elementos = new MenuItem[cantidad];

	/* ---------------------------------------------------------------
	 * Métodos
	 * ------------------------------------------------------------ */

	/**
	 * Constructor por defecto
	 *
	 * @param nombre      nombre del elemento
	 * @param descripcion descripción del elemento
	 */
	public Menu(String nombre, String descripcion) {
		// Llamamos al constructor de la clase padre
		super(nombre, descripcion, null);
		// Por defecto el menu siempre tendrá un elemento por defecto
		// llamado salir
		elementos[0] = new MenuItem("Salir", this::salirAccion);
	}

	/**
	 * Constructor secundario
	 *
	 * @param nombre nombre del elemento
	 */
	public Menu(String nombre) {
		this(nombre, "");
	}

	/* ---------------------------------------------------------------
	 * Métodos
	 * ------------------------------------------------------------ */

	/**
	 * Agrega un elemento al menu
	 *
	 * @param elemento El elemento que se desea agregar
	 */
	public void agregarElemento(MenuItem elemento) {
		// Redimensionamos el array
		MenuItem ultimoElemento = elementos[cantidad - 1];
		MenuItem[] elementosTmp = new MenuItem[++cantidad];

		// Copiamos el contenido del array al nuevo
		if (elementos.length > 1) {
			System.arraycopy(elementos, 0, elementosTmp, 0, elementos.length - 1);
		}

		elementosTmp[cantidad - 2] = elemento;
		elementosTmp[cantidad - 1] = ultimoElemento;
		elementos = elementosTmp;
	}

	/**
	 * Elimina un elemento del menu
	 *
	 * @param elemento el elemento que se desea eliminar
	 */
	public void quitarElemento(MenuItem elemento) {
		int indice = -1;
		// Verificamos en que índice se encuentra el elemento
		for (int i = 0; i < elementos.length; ++i) {
			if (elementos[i].equals(elemento)) {
				indice = i;
				break;
			}
		}

		// Verificamos que el índice no sea negativo
		// si es negativo, entonces se dice que el elemento no existe.
		// Esto también aplica para el último elemento, debido a que este elemento
		// está reservado para la opción salir
		if (indice == -1 || indice == cantidad - 1) return;

		// Creamos el nuevo array con el tamaño deseado
		MenuItem[] elementosTmp = new MenuItem[--cantidad];

		// Realizamos la copia de elementos por la izquierda
		System.arraycopy(elementos, 0, elementosTmp, 0, indice);
		// Realizamos la copia de elementos por la derecha
		System.arraycopy(elementos, indice + 1, elementosTmp, indice, elementosTmp.length - indice);

		// Remplazamos el array del objeto
		elementos = elementosTmp;
	}

	/**
	 * Mostramos el menu creado
	 *
	 * @param reader la entrada del teclado
	 * @param nivel  el nivel de indentación
	 * @throws IOException error al obtener información del teclado
	 */
	public void mostrarMenu(BufferedReader reader, final int nivel) throws IOException {
		// Cambiamos el estado del menu
		activo = true;

		// Iteramos el menu hasta que se seleccioné la opción salir
		while (activo) {
			// Mostramos los elementos insertados
			for (int i = 0; i < elementos.length; ++i) {
				System.out.println(elementos[i].informacion(i + 1, nivel));
			}
			// Realizamos la pregunta del usuario
			final int indice = EntradaUtils.preguntarNumero(
				"Selecciona una opción",
				"La entrada del dato no es de tipo numérico",
				1, cantidad, reader).intValue();

			// Ejecutamos el elemento seleccionado
			MenuItem elemento = elementos[indice - 1];

			// Si el elemento es otro menu entonces llamar este mismo método
			if (elemento instanceof Menu) {
				((Menu) elemento).mostrarMenu(reader, nivel + 1);
			} else {
				// De lo contrario, solamente ejecutamos la acción si esta existe
				if (elemento.accion != null) elemento.accion.run();
				// Mostramos un mensaje de espera
				if (indice < cantidad) EntradaUtils.esperarEntrada(reader);
			}
		}
	}

	/**
	 * Mostramos el menu creado
	 *
	 * @param reader la entrada del teclado
	 * @throws IOException error al obtener información del teclado
	 */
	public void mostrarMenu(BufferedReader reader) throws IOException {
		mostrarMenu(reader, 0);
	}

	/* ---------------------------------------------------------------
	 * Métodos internos
	 * ------------------------------------------------------------ */

	/**
	 * Este método solo cambia el estado del menú
	 * para salir del bucle
	 */
	private void salirAccion() {
		activo = false;
	}

}
