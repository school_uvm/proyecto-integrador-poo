package school.menu;

public class MenuItem {

	/* ---------------------------------------------------------------
	 * Propiedades
	 * ------------------------------------------------------------ */

	/**
	 * El nombre del elemento actual
	 */
	public final String nombre;

	/**
	 * La descripción del elemento
	 */
	public final String descripcion;

	/**
	 * La acción que se ejecutará al seleccionar
	 * el elemento
	 */
	public Runnable accion;

	/* ---------------------------------------------------------------
	 * Constructores
	 * ------------------------------------------------------------ */

	/**
	 * Constructor por defecto
	 *
	 * @param nombre      el nombre del elemento
	 * @param descripcion la descripción del elemento
	 * @param accion      la acción que se ejecutará
	 */
	public MenuItem(String nombre, String descripcion, Runnable accion) {
		// Inicializamos las propiedades
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.accion = accion;
	}

	public MenuItem(String nombre, String descripcion, RunnableSafe accion) {
		// Inicializamos las propiedades
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.accion = accion;
	}

	/**
	 * Constructor secundario
	 *
	 * @param nombre nombre del elemento
	 * @param action acción que se ejecutará
	 */
	public MenuItem(String nombre, Runnable action) {
		this(nombre, "", action);
	}

	public MenuItem(String nombre, RunnableSafe action) {
		this(nombre, "", action);
	}

	/**
	 * Constructor secundario
	 *
	 * @param nombre nombre del elemento
	 */
	public MenuItem(String nombre) {
		this(nombre, "", null);
	}

	/* ---------------------------------------------------------------
	 * Métodos
	 * ------------------------------------------------------------ */

	/**
	 * Devolvemos la información del objeto actual con el índice asignado
	 *
	 * @param indice el índice el elemento
	 * @param nivel  nivel de indentación
	 * @return la información del objeto actual
	 */
	public String informacion(int indice, int nivel) {
		// Generamos un elemento para generar el contenido
		StringBuilder builder = new StringBuilder();
		String nivelStr = nivel <= 0 ? "" : "\t".repeat(nivel);

		// Insertamos el nivel del elemento
		builder.append(nivelStr);

		// Verificamos que el indice no sea negativo
		if (indice >= 0)
			builder.append(indice < 10 ? '0' : '\0').append(indice)
				.append(" - ");

		// Insertamos el nombre del elemento
		builder.append(nombre);

		// Verificamos que la descripción no este vacía
		if (!descripcion.isBlank())
			builder.append(": ")
				.append(descripcion);

		// Devolvemos el resultado
		return builder.toString();
	}

	/**
	 * Devolvemos la información del objeto actual con el índice asignado
	 *
	 * @param indice el índice el elemento
	 * @return la información del objeto actual
	 */
	public String informacion(int indice) {
		return informacion(indice, 0);
	}

	/**
	 * Devolvemos la información del objeto actual sin el índice
	 *
	 * @return la información del objeto actual
	 */
	public String informacion() {
		return informacion(-1);
	}

}
