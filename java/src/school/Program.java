package school;

import school.elementos.Alumno;
import school.elementos.Materia;
import school.elementos.Profesor;
import school.elementos.base.Persona;
import school.error.InfoException;
import school.menu.OpcionEnum;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

import static school.Mensajes.*;
import static school.utilidades.EntradaUtils.*;

public class Program {

	/**
	 * Current object logging system
	 */
	private static final Logger LOG = Logger.getLogger(Program.class.getName());

	/**
	 * Maxima cantidad de profesores
	 */
	private static final int MAX_PROFESORES = 3;

	/**
	 * Maxima cantidad de materias
	 */
	private static final int MAX_MATERIAS = 4;

	/**
	 * Maxima cantidad de alumnos
	 */
	private static final int MAX_ALUMNOS = 3;

	/**
	 * Entrada del teclado global.
	 * <p>
	 * Como las acciones requieren de este objeto, es necesario
	 * crear una instancia accesible desde todos lados.
	 */
	private static BufferedReader globalReader;

	/**
	 * Método principal del programa
	 *
	 * @param args argumentos del programa
	 */
	public static void main(String[] args) {
		// Utilizamos los elementos [AutoCloseables] para evitar escribir más código
		try (var reader = new InputStreamReader(System.in);
			 var buffered = new BufferedReader(reader)) {
			// Ejecutamos el método que nos interesa
			mainImpl(buffered);
		} catch (Exception e) {
			LOG.log(Level.SEVERE, "Ocurrió un error en el programa", e);
		}
	}

	/**
	 * Método principal con posibilidad de errores
	 *
	 * @param reader entrada del teclado
	 * @throws IOException error al leer el teclado
	 */
	private static void mainImpl(BufferedReader reader) throws Exception {
		// Creamos el menú principal
		globalReader = reader;
		MenuPrincipal menuPrincipal = new MenuPrincipal();

		// Iniciamos las acciones de los profesores
		menuPrincipal.inicializarProfesores(Program::agregarProfesor, Program::eliminarProfesor,
											Program::mostrarProfesores);
		// Iniciamos las acciones de las materias
		menuPrincipal.inicializarMaterias(Program::agregarMateria, Program::eliminarMateria,
										  Program::mostrarMaterias);
		// Iniciamos las acciones de los alumnos
		menuPrincipal.inicializarAlumnos(Program::agregarAlumno, Program::eliminarAlumno, Program::mostrarAlumnos,
										 Program::alumnoAgregarMateria, Program::alumnoQuitaMateria);

		menuPrincipal.mostrarMenu(reader);
	}

	/* ---------------------------------------------------------------
	 * Propiedades
	 * ------------------------------------------------------------ */

	/**
	 * Lista de profesores registrados
	 */
	private static final List<Profesor> profesores =
		new ArrayList<>();

	/**
	 * Lista de materias registradas
	 */
	private static final List<Materia> materias =
		new ArrayList<>();

	/**
	 * Lista de alumnos registrados
	 */
	private static final List<Alumno> alumnos =
		new ArrayList<>();

	/* ---------------------------------------------------------------
	 * Profesores
	 * ------------------------------------------------------------ */

	/**
	 * Agregamos un profesor a la lista
	 *
	 * @throws Exception Error al realizar la acción
	 */
	private static void agregarProfesor() throws Exception {
		// Para esta etapa el límite de profesores es 3
		if (profesores.size() >= MAX_PROFESORES) throw new InfoException("Se alcanzó el limite máximo de profesores");
		// Obtenemos el último elemento y verificamos su id
		long ultimoId = profesores.isEmpty() ? 0 : profesores.get(profesores.size() - 1).getId();
		// Iniciamos las preguntas para el usuario
		Profesor elemento = new Profesor(
			ultimoId + 1,
			preguntarTexto(msgIn("el", "Profesor", "nombre"), MSG_ERR_VACIO, globalReader),
			preguntarTexto(msgIn("el", "Profesor", "apellido"), MSG_ERR_VACIO, globalReader),
			preguntarNumero(msgIn("el", "Profesor", "edad"), MSG_ERR_NUMERO, globalReader),
			preguntarTipoEnum(msgIn("el", "Profesor", "sexo"), MSG_ERR_LISTA, Persona.Sexo.class, globalReader),
			preguntarTexto(msgIn("el", "Profesor", "profesión"), MSG_ERR_VACIO, globalReader),
			preguntarTexto(msgIn("el", "Profesor", "cédula"), MSG_ERR_VACIO, globalReader)
		);
		// Verificamos que la cédula o el ID no se repita
		Stream<Profesor> profesorStream = profesores.stream();
		boolean encontrado = profesorStream.anyMatch(
			it -> it.getCedula().equals(elemento.getCedula()) || it.getId() == elemento.getId());
		// Mostramos mensaje de error si el profesor ya existe
		if (encontrado) throw new InfoException("La información ingresada ya existe");
		// Agregamos el elemento
		profesores.add(elemento);
	}

	/**
	 * Eliminamos un profesor de la lista
	 *
	 * @throws Exception Error al realizar la acción
	 */
	private static void eliminarProfesor() throws Exception {
		// Para poder eliminar un profesor primero debe de existir al menos uno
		if (profesores.isEmpty()) throw new InfoException("No existe ningún profesor aún");

		// Preguntamos al usuario por algún profesor que desea eliminar
		Profesor profesor = preguntarCollection("Seleccione a un profesor", MSG_ERR_LISTA, profesores, globalReader);

		// Preguntamos al usuario si está realmente seguro de eliminar al profesor
		OpcionEnum verificar = preguntarTipoEnum(MSG_VERIFCACION, MSG_ERR_LISTA, OpcionEnum.class, globalReader);
		if (verificar == OpcionEnum.NO) throw new InfoException(MSG_ERR_CANCEL);

		// Eliminamos al profesor por su índice en la lista
		profesores.remove(profesor);
		// Eliminar también el profesor de las otras listas
		materias.stream()
			.filter(it -> it.getProfesor() != null)
			.filter(it -> it.getProfesor().getId() == profesor.getId())
			.forEach(it -> it.setProfesor(null));

		// Mensaje informativo
		System.out.println("Profesor eliminado con éxito");
	}

	/**
	 * Mostramos todos los profesores
	 *
	 * @throws Exception Error al realizar la acción
	 */
	private static void mostrarProfesores() throws Exception {
		// Si no hay ningún profesor registrado
		// mostramos un mensaje y no realizamos ninguna impresión de los elementos
		if (profesores.isEmpty()) throw new InfoException("No hay ningún profesor registrado");

		// Mostramos los elementos de la lista
		for (Profesor profesor : profesores) {
			System.out.println(profesor.getInformacion());
		}
	}

	/* ---------------------------------------------------------------
	 * Materias
	 * ------------------------------------------------------------ */

	/**
	 * Agregamos la materia a la lista de materias
	 *
	 * @throws Exception Error al realizar la acción
	 */
	private static void agregarMateria() throws Exception {
		// Para esta etapa el límite de materias es 4
		if (materias.size() >= MAX_MATERIAS) throw new InfoException("Se alcanzó el limite máximo de materias");
		// Para agregar materias necesitamos saber si existe algún profesor
		if (profesores.isEmpty()) throw new InfoException("Debe haber al menos un profesor registrado");
		// Iniciamos las preguntas para el usuario
		long ultimoId = materias.isEmpty() ? 0 : materias.get(materias.size() - 1).getId();
		Materia elemento = new Materia(
			ultimoId + 1,
			preguntarTexto(msgIn("el", "Materia", "nombre", "de la"), MSG_ERR_VACIO, globalReader),
			preguntarNumero(msgIn("los", "Materia", "créditos", "de la"), MSG_ERR_NUMERO, globalReader).intValue(),
			preguntarCollection(msgIn("el", "Materia", "profesor", "de la"), MSG_ERR_LISTA, profesores, globalReader)
		);
		// Verificamos que la materia no exista
		Stream<Materia> materiaStream = materias.stream();
		boolean encontrado = materiaStream.anyMatch(
			it -> it.getId() == elemento.getId() || it.getNombre().equals(elemento.getNombre()));
		// Mostramos mensaje de error si el profesor ya existe
		if (encontrado) throw new InfoException("La información ingresada ya existe");
		// Agregamos el elemento
		materias.add(elemento);
	}

	/**
	 * Eliminamos el profesor de la lista de profesores
	 *
	 * @throws Exception Error al realizar la acción
	 */
	private static void eliminarMateria() throws Exception {
		// Para poder eliminar una materia primero debe de existir al menos una
		if (materias.isEmpty()) throw new InfoException("No existe ninguna materia aún");

		// Preguntamos al usuario por alguna materia que desea eliminar
		Materia materia = preguntarCollection("Seleccione una materia", MSG_ERR_LISTA, materias, globalReader);

		// Preguntamos al usuario si está realmente seguro de eliminar la materia
		OpcionEnum verificar = preguntarTipoEnum(MSG_VERIFCACION, MSG_ERR_LISTA, OpcionEnum.class, globalReader);
		if (verificar == OpcionEnum.NO) throw new InfoException(MSG_ERR_CANCEL);

		// Eliminamos la materia por su objeto
		materias.remove(materia);
		// Eliminar también las materias de las otras listas
		alumnos.forEach(it -> it.getMaterias().remove(materia));

		// Mensaje informativo
		System.out.println("Profesor eliminado con éxito");
	}

	/**
	 * Mostramos todas las materias
	 *
	 * @throws Exception Error al realizar la acción
	 */
	private static void mostrarMaterias() throws Exception {
		// Si no hay ninguna materia registrada
		// mostramos un mensaje y no realizamos ninguna impresión de los elementos
		if (materias.isEmpty()) throw new InfoException("No hay ninguna materia registrada");

		// Mostramos los elementos de la lista
		for (Materia materia : materias) {
			System.out.println(materia.getInformacion());
		}
	}

	/* ---------------------------------------------------------------
	 * Alumnos
	 * ------------------------------------------------------------ */

	/**
	 * Agregamos un alumno a la lista de alumnos
	 *
	 * @throws Exception Error al realizar la acción
	 */
	private static void agregarAlumno() throws Exception {
		// Para esta etapa el límite de alumnos es 3
		if (alumnos.size() >= MAX_ALUMNOS) throw new InfoException("Se alcanzó el limite máximo de alumnos");
		// Iniciamos las preguntas para el usuario
		long ultimoId = alumnos.isEmpty() ? 0 : alumnos.get(alumnos.size() - 1).getId();
		Alumno elemento = new Alumno(
			ultimoId + 1,
			preguntarTexto(msgIn("el", "Alumno", "nombre", "el"), MSG_ERR_VACIO, globalReader),
			preguntarTexto(msgIn("el", "Alumno", "apellido", "el"), MSG_ERR_VACIO, globalReader),
			preguntarNumero(msgIn("el", "Alumno", "edad", "la"), MSG_ERR_NUMERO, globalReader),
			preguntarTipoEnum(msgIn("el", "Alumno", "sexo", "el"), MSG_ERR_LISTA, Persona.Sexo.class, globalReader),
			preguntarNumero(msgIn("el", "Alumno", "noControl", "el"), MSG_ERR_NUMERO, globalReader),
			preguntarNumero(msgIn("el", "Alumno", "semestre", "el"), MSG_ERR_NUMERO, globalReader),
			null
		);
		// Verificamos que la materia no exista
		Stream<Alumno> alumnoStream = alumnos.stream();
		boolean encontrado = alumnoStream.anyMatch(
			it -> it.getId() == elemento.getId() || it.getNombre().equals(elemento.getNombre()));
		// Mostramos mensaje de error si el profesor ya existe
		if (encontrado) throw new InfoException("La información ingresada ya existe");
		// Agregamos el elemento
		alumnos.add(elemento);
	}

	/**
	 * Eliminamos un alumno de la lista de alumnos
	 *
	 * @throws Exception Error al realizar la acción
	 */
	private static void eliminarAlumno() throws Exception {
		// Para poder eliminar un alumno primero debe de existir al menos uno
		if (alumnos.isEmpty()) throw new InfoException("No existe ningún alumno aún");

		// Preguntamos al usuario por algún alumno que desea eliminar
		Alumno alumno = preguntarCollection("Seleccione un alumno", MSG_ERR_LISTA, alumnos, globalReader);

		// Preguntamos al usuario si está realmente seguro de eliminar al alumno
		OpcionEnum verificar = preguntarTipoEnum(MSG_VERIFCACION, MSG_ERR_LISTA, OpcionEnum.class, globalReader);
		if (verificar == OpcionEnum.NO) throw new InfoException(MSG_ERR_CANCEL);

		// Eliminamos el alumno por su objeto
		alumnos.remove(alumno);

		// Mensaje informativo
		System.out.println("Profesor eliminado con éxito");
	}

	/**
	 * Mostramos todas los alumnos
	 *
	 * @throws Exception Error al realizar la acción
	 */
	private static void mostrarAlumnos() throws Exception {
		// Si no hay ningún alumno registrado
		// mostramos un mensaje y no realizamos ninguna impresión de los elementos
		if (alumnos.isEmpty()) throw new InfoException("No hay ningún alumno registrado");

		// Mostramos los elementos de la lista
		for (Alumno alumno : alumnos) {
			System.out.println(alumno.getInformacion());
		}
	}

	/**
	 * Agregamos una materia al alumno seleccionado
	 *
	 * @throws Exception Error al realizar la acción
	 */
	private static void alumnoAgregarMateria() throws Exception {
		// Si no hay ningún alumno registrado
		// mostramos un mensaje y no realizamos ninguna impresión de los elementos
		if (alumnos.isEmpty()) throw new InfoException("No hay ningún alumno registrado");
		// Si no hay ninguna materia registrada
		// mostramos un mensaje y no realizamos ninguna impresión de los elementos
		if (materias.isEmpty()) throw new InfoException("No hay ninguna materia registrada");

		// Obtenemos el elemento al que se quiere agregar la materia
		Alumno alumno = preguntarCollection("Selecciona al alumno que desea agregar la materia", MSG_ERR_LISTA,
											alumnos, globalReader);
		OpcionEnum agregarMas = OpcionEnum.SI;
		do {
			Materia materia = preguntarCollection("Selecciona la materia que desea agregar", MSG_ERR_LISTA,
												  materias, globalReader);
			// Para prevenir errores, verificaremos que haya materias disponibles para el usuario
			if (alumno.getMaterias().size() >= materias.size())
				throw new InfoException("Ya no hay materias para registrar");
			// Verificamos si la materia ya existe
			Stream<Materia> materiaStream = alumno.getMaterias().stream();
			if (materiaStream.anyMatch(it -> it.equals(materia))) {
				LOG.log(Level.SEVERE, "La materia ya se encuentra registrada");
				continue;
			}
			// Agregamos la materia a la lista
			alumno.getMaterias().add(materia);
			// Preguntamos si quiere agregar más materias
			agregarMas = preguntarTipoEnum("Desea agregar más materias", MSG_ERR_LISTA,
										   OpcionEnum.class, globalReader);
		} while (agregarMas == OpcionEnum.SI);
	}

	/**
	 * Eliminamos una materia al alumno seleccionado
	 *
	 * @throws Exception Error al realizar la acción
	 */
	private static void alumnoQuitaMateria() throws Exception {
		// Si no hay ningún alumno registrado
		// mostramos un mensaje y no realizamos ninguna impresión de los elementos
		if (alumnos.isEmpty()) throw new InfoException("No hay ningún alumno registrado");
		// Si no hay ninguna materia registrada
		// mostramos un mensaje y no realizamos ninguna impresión de los elementos
		if (materias.isEmpty()) throw new InfoException("No hay ninguna materia registrada");

		// Obtenemos el elemento al que se quiere agregar la materia
		Alumno alumno = preguntarCollection("Selecciona al alumno que desea quitar la materia", MSG_ERR_LISTA,
											alumnos, globalReader);
		// Preguntamos por la materia que quiere eliminar
		OpcionEnum quitarMas;
		do {
			// Verificamos si el alumno tiene registrada alguna materia
			if (alumno.getMaterias().isEmpty())
				throw new InfoException("El alumno seleccionado no tiene ninguna materia registrada");
			Materia materia = preguntarCollection("Selecciona la materia que desea quitar", MSG_ERR_LISTA,
												  alumno.getMaterias(), globalReader);
			// Eliminamos la materia
			alumno.getMaterias().remove(materia);
			// Preguntamos si quiere quitar más materias
			quitarMas = preguntarTipoEnum("Desea quitar más materias", MSG_ERR_LISTA,
										  OpcionEnum.class, globalReader);
		} while (quitarMas == OpcionEnum.SI);
	}

}
