#include <iostream>
#include <memory>
#include <vector>
#include "Persona.h"
#include "Materia.h"

#ifndef CPP_PROYECTO_ALUMNO_H
#define CPP_PROYECTO_ALUMNO_H

/// <summary>
/// Caracter que separa las materias registradas
/// </summary>
inline constexpr char MATERIA_SEPARATOR = ',';

class Alumno : public Persona {
private:

	/// <summary>
	/// Numero de control escolar
	/// </summary>
	long _noControl;

	/// <summary>
	/// Semestre actual del alumno
	/// </summary>
	short _semestre;

	/// <summary>
	/// Vector con todas las materias que cursará el alumno.
	/// El vector es de <see chref="std::shared_ptr"/> para evitar crear muchos
	/// objetos y simplemente utilizar una referencia de las materias generales
	/// </summary>
	std::vector<std::shared_ptr<Materia>> _materias;

public:

	/// <summary>
	/// Constructor con todas las propiedades
	/// </summary>
	/// <param name="nombre">Nombre del alumno</param>
	/// <param name="apellido">Apellido del alumno</param>
	/// <param name="edad">Edad del alumno</param>
	/// <param name="sexo">Sexo del alumno</param>
	/// <param name="noControl">Numero de control del alumno</param>
	/// <param name="semestre">Semestre actual del alumno</param>
	/// <param name="materias">Materias que cursará el alumno</param>
	Alumno(
		std::string_view nombre,
		std::string_view apellido,
		int edad,
		Sexo sexo,
		long noControl,
		short semestre,
		std::vector<std::shared_ptr<Materia>> materias);

	/// <summary>
	/// Devuelve el numero de control del alumno
	/// </summary>
	/// <returns>El numero de control</returns>
	const long getNoControl() const;

	/// <summary>
	/// Devuelve el semestre del alumno
	/// </summary>
	/// <returns>El semestre del alumno</returns>
	const short getSemestre() const;

	/// <summary>
	/// Devuelve las materias del alumno
	/// </summary>
	/// <returns>Todas las materias</returns>
	const std::vector<std::shared_ptr<Materia>> getMaterias() const;

	/// <summary>
	/// Agrega una nueva materia al alumno
	/// </summary>
	/// <param name="materia">La mater�a a ingresar</param>
	/// <returns>Retorna true si la materia se agrego correctamente o false de otra forma</returns>
	bool agregarMateria(std::shared_ptr<Materia> materia);

};

#endif // !CPP_PROYECTO_ALUMNO_H