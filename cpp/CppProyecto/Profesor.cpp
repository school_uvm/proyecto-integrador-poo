#include "Profesor.h"
#include "Utilidades.h"

/// <summary>
/// Total del elementos del objeto
/// </summary>
const size_t OBJETO_ELEMENTOS = 6;

Profesor::Profesor(
	std::string_view nombre,
	std::string_view apellido,
	int edad,
	Sexo sexo,
	std::string_view profesion,
	std::string_view cedula) : Persona(nombre, apellido, edad, sexo)
{
	_profesion = profesion;
	_cedula = cedula;
}

const std::string Profesor::getProfesion() const
{
	return _profesion;
}

const std::string Profesor::getCedula() const
{
	return _cedula;
}