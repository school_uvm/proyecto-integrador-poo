#include <iostream>

#ifndef CPP_PROYECTO_PERSONA_H
#define CPP_PROYECTO_PERSONA_H

/// <summary>
/// Enumerador que identifica el sexo de una persona
/// </summary>
enum Sexo : char
{
	MASCULINO = 'M',
	FEMENINO = 'F'
};

/// <summary>
/// Objeto base, que representa a una persona en general.
/// Abstraccion para crear diferentes objetos con la misma informacion de una persona
/// </summary>
class Persona {
private:

	/// <summary>
	/// Nombre que identifica la persona
	/// </summary>
	std::string _nombre;

	/// <summary>
	/// Apellido que identifica la persona
	/// </summary>
	std::string _apellido;

	/// <summary>
	/// Edad de la persona en a�os
	/// </summary>
	int _edad;

	/// <summary>
	/// El sexo de la persona
	/// </summary>
	Sexo _sexo;

public:

	/// <summary>
	/// Constructor con todas las propiedades
	/// </summary>
	/// <param name="nombre">Nombre de la persona</param>
	/// <param name="apellido">Apellido de la persona</param>
	/// <param name="edad">Edad de la persona</param>
	/// <param name="sexo">Sexo de la persona</param>
	/// <see chref="Sexo"/>
	Persona(std::string_view nombre, std::string_view apellido, int edad, Sexo sexo);

	/// <summary>
	/// Destructor de la clase.
	/// Es virtual para que el compilador sepa que esta clase es abstracta
	/// y no se puedan crear instancias de esta
	/// </summary>
	virtual ~Persona() = default;

	/// <summary>
	/// Obtenemos el nombre de la persona
	/// </summary>
	/// <returns>El nombre de la persona</returns>
	const std::string& getNombre() const;

	/// <summary>
	/// Obtenemos el apellido de la persona
	/// </summary>
	/// <returns>El apellido de la persona</returns>
	const std::string& getApellido() const;

	/// <summary>
	/// Obtenemos la edad de la persona
	/// </summary>
	/// <returns>La edad de la persona</returns>
	const int getEdad() const;

	/// <summary>
	/// Obtenemos el sexo de la persona
	/// </summary>
	/// <returns>El sexo de la persona</returns>
	const Sexo getSexo() const;

};

#endif // !CPP_PROYECTO_PERSONA_H