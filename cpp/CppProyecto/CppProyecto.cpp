#include <iostream>

#include "Materia.h"
#include "Profesor.h"
#include "Alumno.h"

int main() {
	auto profesor = std::make_shared<Profesor>("Marco", "Galvan", 25, MASCULINO, "Ingeniero", "ASADQWNKADAD");
	auto materia = std::make_shared<Materia>("Programacion en C++", 10, profesor);

	std::vector<std::shared_ptr<Materia>> materias = { materia };
	auto alumno = std::make_shared<Alumno>("Brian", "Alvarez", 25, MASCULINO, 12, 3, materias);

	std::cout << profesor << '\n';
	std::cout << materia << '\n';
	std::cout << alumno << '\n';

	return 0;
}