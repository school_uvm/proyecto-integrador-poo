#include <iostream>
#include <vector>

#ifndef CPP_PROYECTO_UTILIDADES_H
#define CPP_PROYECTO_UTILIDADES_H

/// <summary>
/// Elimina los espacios en blanco a la izquierda del contenido
/// </summary>
/// <param name="content">El contenindo que se quiere analizar</param>
/// <returns>Un string nuevo sin los espacios</returns>
std::string lTrim(const std::string& content);

/// <summary>
/// Elimina los espacios en blanco a la derecha del contenido
/// </summary>
/// <param name="content">El contenindo que se quiere analizar</param>
/// <returns>Un string nuevo sin los espacios</returns>
std::string rTrim(const std::string& content);

/// <summary>
/// Elimina los espacios en blanco tanto por la derecha como la isquierda
/// </summary>
/// <param name="content">El contenindo que se quiere analizar</param>
/// <returns>Un string nuevo sin los espacios</returns>
std::string stringTrim(const std::string& content);

/// <summary>
/// Separa el texto en multiples elementos dependiendo del delimitador
/// </summary>
/// <param name="content">El contenido que se quiere analizar</param>
/// <param name="delim">Caracter que delimita el contenido</param>
/// <returns>Un vector con todas las partes separadas</returns>
const std::vector<std::string> stringSplit(const std::string& content, char delim);

#endif // !CPP_PROYECTO_UTILIDADES_H
