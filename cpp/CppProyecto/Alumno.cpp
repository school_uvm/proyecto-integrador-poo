#include "Alumno.h"

Alumno::Alumno(
	std::string_view nombre,
	std::string_view apellido,
	int edad,
	Sexo sexo,
	long noControl,
	short semestre,
	std::vector<std::shared_ptr<Materia>> materias) : Persona(nombre, apellido, edad, sexo)
{
	_noControl = noControl;
	_semestre = semestre;
	_materias = materias;
}

const long Alumno::getNoControl() const
{
	return _noControl;
}

const short Alumno::getSemestre() const
{
	return _semestre;
}

const std::vector<std::shared_ptr<Materia>> Alumno::getMaterias() const
{
	std::vector<std::shared_ptr<Materia>> copy = _materias;
	return copy;
}

bool Alumno::agregarMateria(std::shared_ptr<Materia> materia)
{
	try {
		_materias.push_back(materia);
	}
	catch (const std::exception& e) {
		std::cout << e.what() << '\n';
		return false;
	}

	return true;
}