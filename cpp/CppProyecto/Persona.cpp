#include "Persona.h"

Persona::Persona(std::string_view nombre, std::string_view apellido, int edad, Sexo sexo)
{
	_nombre = nombre;
	_apellido = apellido;
	_edad = edad;
	_sexo = sexo;
}

const std::string& Persona::getNombre() const
{
	return _nombre;
}

const std::string& Persona::getApellido() const
{
	return _apellido;
}

const int Persona::getEdad() const
{
	return _edad;
}

const Sexo Persona::getSexo() const
{
	return _sexo;
}