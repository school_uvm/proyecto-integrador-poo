#include <string>
#include <vector>
#include "Utilidades.h"

bool isSpaceCharDetection(unsigned char item) {
	return !std::isspace(item);
}

std::string lTrim(const std::string& content)
{
	std::string copy{ content };
	// Eliminamos todos los elementos a la izquierda que es un espacio
	copy.erase(copy.begin(), std::find_if(copy.begin(), copy.end(), isSpaceCharDetection));
	return copy;
}

std::string rTrim(const std::string& content)
{
	std::string copy{ content };
	// Delete all empty spaces
	copy.erase(std::find_if(copy.rbegin(), copy.rend(), isSpaceCharDetection).base(), copy.end());
	return copy;
}

std::string stringTrim(const std::string& content)
{
	return lTrim(rTrim(content));
}

const std::vector<std::string> stringSplit(const std::string& content, char delim)
{
	// Generamos un resultado con todos los elementos
	std::vector<std::string> elements = {};
	std::string current{ "" };

	// Iterate over string 
	for (size_t i = 0; i < content.size(); ++i) {
		// Verificar si el elemento actual es el delimitador seleccionado
		if (content[i] != delim) {
			current.push_back(content[i]);
			continue;
		}
		// Obtenemos el rango de los elementos que tuvieron una coincidencia
		elements.push_back(current);
		current = "";
	}

	return elements;
}
