#include "Materia.h"

Materia::Materia(std::string_view nombre, int creditos, const std::shared_ptr<Profesor>& profesor)
{
	_nombre = nombre;
	_creditos = creditos;
	_profesor = profesor;
}

const std::string Materia::getNombre() const
{
	return _nombre;
}

const int Materia::getCreditos() const
{
	return _creditos;
}

const std::shared_ptr<Profesor>& Materia::getProfesor()
{
	return _profesor;
}