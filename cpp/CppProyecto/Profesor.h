#include <iostream>
#include "Persona.h"

#ifndef CPP_PROYECTO_PROFESOR_H
#define CPP_PROYECTO_PROFESOR_H

class Profesor : public Persona
{
private:

	/// <summary>
	/// Titulo o profesion del profesor
	/// </summary>
	std::string _profesion;

	/// <summary>
	/// Cedula profesional
	/// </summary>
	std::string _cedula;

public:

	Profesor(
		std::string_view nombre, 
		std::string_view apellido, 
		int edad, 
		Sexo sexo, 
		std::string_view profesion,
		std::string_view cedula);

	/// <summary>
	/// Devuelve la profesion del profesor
	/// </summary>
	/// <returns></returns>
	const std::string getProfesion() const;

	/// <summary>
	/// Devuelve la cedula del profesor
	/// </summary>
	/// <returns></returns>
	const std::string getCedula() const;
};

#endif // !CPP_PROYECTO_PROFESOR_H

