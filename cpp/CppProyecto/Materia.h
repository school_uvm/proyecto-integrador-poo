#include <iostream>
#include <memory>
#include "Profesor.h"

#ifndef CPP_PROYECTO_MATERIA_H
#define CPP_PROYECTO_MATERIA_H

class Materia
{
private:

	/// <summary>
	/// Nombre de la materia
	/// </summary>
	std::string _nombre;

	/// <summary>
	/// Total de creditos de la materia
	/// </summary>
	int _creditos;

	/// <summary>
	/// El profesor que impartir� la materia
	/// </summary>
	std::shared_ptr<Profesor> _profesor;

public:

	/// <summary>
	/// Constructor principal
	/// </summary>
	/// <param name="nombre">Nombre de la materia</param>
	/// <param name="creditos">Creditos para aprovar la materia</param>
	/// <param name="profesor">El profesor que imparte la materia</param>
	Materia(std::string_view nombre, int creditos, const std::shared_ptr<Profesor>& profesor);

	/// <summary>
	/// Devuelve el nombre de la materia
	/// </summary>
	/// <returns></returns>
	const std::string getNombre() const;

	/// <summary>
	/// Devuelve el total de creditos para acreditar la materia
	/// </summary>
	/// <returns></returns>
	const int getCreditos() const;

	/// <summary>
	/// Devuelve el profesor que imparte la materia
	/// </summary>
	/// <returns></returns>
	const std::shared_ptr<Profesor>& getProfesor();

};

#endif // !CPP_PROYECTO_MATERIA_H

